Systemressourcen ermitteln
==========================
Um Systemressourcen wie Speicherplatz, RAM usw. in Python auszulesen, gibt es folgende unterschiedliche Möglichkeiten:
- Mit der Python-Standardbibliotheken _**shutil**_, kann man sich die Speicherkapazität eines Laufwerkes anzeigen lassen. 
Die Funktion dafür lautet shutil.disk_usage(). Siehe [Python-Dokumentation](https://docs.python.org/3/library/shutil.html#module-shutil).

- Die Python-Standardbibliothek **_subprocess_** bietet die Möglichkeit aus einem Python-Skript heraus neue Prozesse zu starten und 
das Ergebnis weiter verarbeiten zu können. [Offizielle Python-Dokumentation](https://docs.python.org/3.7/library/subprocess.html#module-subprocess).
Für die Monitoring-Aufgabe können wir das nutzen, um mithilfe von Shell-Kommandos wie _free_ , _top_ in Linux oder _tasklist.exe_ in Windows. 
Unter Linux kann man z.B. den Shell Befehl 
```
free -t -m
```
nutzen. *Free* gibt die Arbeitsspeicherauslastung aus. Die Optionen -t und -m bedeuten dabei: Ausgabe der Daten in Megabyte 
und Anzeige einer Zeile mit den Gesamtübersichten (total). 
In Python wird das mit *subprocess* wie folgt umgesetzt:
```
tot_m, used_m, free_m =subprocess.getoutput('free -t -m|grep "Total"').split()[1:]
```
Weitere Beispiele finden Sie auch im [Arbeitsblatt Verzweigungen und Schleifen](https://moodle.itech-bs14.de/pluginfile.php/34514/mod_resource/content/10/Arbeitsblatt_Kontrollstrukturen.pdf) des Moduls Monitoring in Python

- Die Bibliothek _**psutil**_ ist keine Python-Standardbibliothek und muß daher gesondert installiert werden. Dazu sind 
Admin-Rechte erforderlich.   
Die Bibliothek psutil (process and system utilities) bietet plattformübergreifende Funktionen zum Auslesen von Informationen über
laufende Prozesse und Systemauslastung(CPU, memory, disks, network, sensors) in Python. Folgende Betriebssysteme werden aktuell unterstützt: 
Linux, Windows, macOS, FreeBSD, OpenBSD, NetBSD, Sun Solaris und AIX. 
Die Bibliothek ist sehr hilfreich für System-Monitoring. Eine ausführliche Dokumentation des Funktionsumfangs mit Anwendungsbeispielen und
Hinweise zur Installation finden sich auf [github](https://github.com/giampaolo/psutil)

Systemwerte auslesen mit ctypes
-------------------------------
Das Modul WindowsResources.py zeigt exemplarisch wie unter Windows RAM-Werte ausgelesen werden können, ohne Verwendung von Drittbibliotheken wie psutil. Dazu wird die Python Bibliothek ctypes verwendet. 
Das Modul ctypes stellt C-kompatible Datentypen zur Verfügung und unterstützt DLL-Funktionsaufrufe. 
Wie können Sie das Modul verwenden?
1. Kopieren Sie die Datei WindowsResources.py in ihr Arbeitsverzeichnis
2. Importieren Sie das Modul in Python mit: _import WindowsResources_
3. Sie können nun die Funktion _getMemory()_ aufrufen. Die Funktionen gibt ein Tupel mit 3 Werten zurück: dwTotalPhys, dwAvailPhys, dwMemoryLoad,
wobei dwMemoryLoad der genutzte Speicher in % darstellt.

Beispiel:
```
import WindowsResources
memory = WindowsResources.getMemory()
print(memory['dwMemoryLoad'])
```

**Quellen:**

[offizielle Python Dokumentation](https://docs.python.org/3.7/library/ctypes.html)

[Stackoverflow:Virtual Memory report with ctypes](https://stackoverflow.com/questions/38247958/incorrect-virtual-memory-report-using-ctypes-in-windows)

   